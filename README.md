Menage containers:
docker exec -it ubuntu_bash bash

Run App in the docker
docker-compose up -d

Stop docker containers
docker-compose stop

Remove all containers
docker-compose down